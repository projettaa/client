'use strict';

var gulp   = require('gulp'),
    concat = require('gulp-concat'),
    minify = require('gulp-minify'),
    sass   = require('gulp-sass'),
    merge  = require('merge-stream');

var paths = {
    css      : ['css/main.scss'],
    cssConcat: [
        'bower_components/gentelella/vendors/iCheck/skins/flat/green.css',
        //'bower_components/datatables/media/css/jquery.dataTables.css',
        // 'bower_components/datatables.net-bs/css/dataTables.bootstrap.css',
        'bower_components/angular-xeditable/dist/css/xeditable.css',
        'bower_components/angular-flash-alert/dist/angular-flash.css'
    ],
    scripts  : ['js/main.js', 'js/index.js', 'js/controllers/**', 'js/directives/**', 'js/filters/**', 'js/models/**', 'js/config/**', 'js/constants/**'],
    vendors  : [
        'bower_components/jquery/dist/jquery.js',
        'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
        'bower_components/gentelella/vendors/fastclick/lib/fastclick.js',
        'bower_components/gentelella/vendors/nprogress/nprogress.js',
        'bower_components/gentelella/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
        'bower_components/gentelella/vendors/Chart.js/dist/Chart.min.js',
        'bower_components/gentelella/vendors/bernii/gauge.js/dist/gauge.min.js',
        'bower_components/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js',
        'bower_components/gentelella/vendors/iCheck/icheck.min.js',
        'bower_components/gentelella/vendors/skycons/skycons.js',
        'bower_components/gentelella/vendors/Flot/jquery.flot.js',
        'bower_components/gentelella/vendors/Flot/jquery.flot.pie.js',
        'bower_components/gentelella/vendors/Flot/jquery.flot.time.js',
        'bower_components/gentelella/vendors/Flot/jquery.flot.stack.js',
        'bower_components/gentelella/vendors/Flot/jquery.flot.resize.js',
        'bower_components/gentelella/vendors/flot/jquery.flot.orderBars.js',
        'bower_components/gentelella/vendors/flot/date.js',
        'bower_components/gentelella/vendors/flot/jquery.flot.spline.js',
        'bower_components/gentelella/vendors/flot/curvedLines.js',
        'bower_components/gentelella/vendors/maps/jquery-jvectormap-2.0.3.min.js',
        'bower_components/gentelella/vendors/moment/moment.min.js',
        'bower_components/gentelella/vendors/datepicker/daterangepicker.js',
        'bower_components/gentelella/vendors/maps/jquery-jvectormap-world-mill-en.js',
        'bower_components/gentelella/vendors/maps/jquery-jvectormap-us-aea-en.js',
        'bower_components/gentelella/vendors/maps/gdp-data.js',
        'bower_components/gentelella/src/js/helper.js',
        'bower_components/angular/angular.js',
        'bower_components/angular-route/angular-route.js',
        'bower_components/angular-resource/angular-resource.js',
        'bower_components/angular-animate/angular-animate.js',
        // 'bower_components/datatables.net/js/jquery.dataTables.js',
        // 'bower_components/datatables.net-bs/js/dataTables.bootstrap.js',
        // 'bower_components/angular-datatables/dist/angular-datatables.js',
        'bower_components/moment/moment.js',
        'bower_components/angular-xeditable/dist/js/xeditable.js',
        'bower_components/angular-flash-alert/dist/angular-flash.js',
        'bower_components/jsog/lib/JSOG.js',
        'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
        'bower_components/angular-ui-mask/dist/mask.js',
        'bower_components/jspdf/dist/jspdf.min.js',
        'bower_components/html2canvas/build/html2canvas.js'
    ],
    images   : [
        'bower_components/gentelella/production/images/**'
    ],
    cssImages: [
        //'bower_components/datatables/media/images/**'
    ],
    fonts    : [
        'bower_components/gentelella/vendors/font-awesome/fonts/**',
        'bower_components/bootstrap-sass/assets/fonts/**'
    ]
};

// css
gulp.task('sass', function () {
    var sassStream = gulp.src(paths.css)
                         .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError));

    var cssStream = gulp.src(paths.cssConcat);

    merge(sassStream, cssStream)
        .pipe(concat('main.css'))
        .pipe(gulp.dest('css/target'));
});

// vendors
gulp.task('vendors', function () {
    gulp.src(paths.vendors)
        .pipe(concat('vendors.js'))
        .pipe(minify({
            ext: {
                src: '-debug.js',
                min: '.min.js'
            }
        }))
        .pipe(gulp.dest('js/target'));
});


// scripts
gulp.task('scripts', function () {
    gulp.src(paths.scripts)
        .pipe(concat('main.js'))
        .pipe(minify({
            ext: {
                src: '-debug.js',
                min: '.min.js'
            }
        }))
        .pipe(gulp.dest('js/target'));
});

// images
gulp.task('images', function () {
    gulp.src(paths.images)
        .pipe(gulp.dest('images'));

    gulp.src(paths.cssImages)
        .pipe(gulp.dest('css/images'));
});

// fonts
gulp.task('fonts', function () {
    gulp.src(paths.fonts)
        .pipe(gulp.dest('css/fonts'));
});

gulp.task('watch', function () {
    gulp.watch(paths.css, ['sass']);
    gulp.watch(paths.scripts, ['scripts']);
});

gulp.task('default', ['sass', 'vendors', 'scripts', 'images', 'fonts']);