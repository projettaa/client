'use strict';

app.directive('liActive', ['$route', function ($route) {
    return {
        restrict   : 'E',
        replace    : true,
        transclude : true,
        scope      : true,
        templateUrl: 'views/templates/li_active.html',
        link       : function (scope, element) {
            scope.route  = element.children('a').attr('href');
            scope.active = function (route) {
                route = route.substr(1);
                if ($route.current && $route.current.$$route) {
                    var currentRoute = $route.current.$$route.originalPath.substr(1);
                    return route === currentRoute || (currentRoute.match(new RegExp(route)) && route !== "");
                }
                return false;
            }
        }
    };
}]);