'use strict';

app.directive('slidingItem', [function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.addClass('slider ' + attrs.state);
            element.on('click', function (e) {
                var $e = angular.element(e.target);
                if (!$e.parents('ul').hasClass('child_menu') || (!$e.children().hasClass('child_menu') && element.is($e.parents('li').first()))) {
                    e.preventDefault();
                    element.toggleClass('closed');
                }
            });

            element.on('$destroy', function () {
                element.off();
            });
        }
    };
}]);