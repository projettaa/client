'use strict';

app.directive('toggleMenu', ['$route', function ($route) {
    return {
        restrict: 'A',
        link    : function (scope, element) {
            element.on('click', function () {
                var $SIDEBAR_MENU = $('#sidebar-menu'),
                    $BODY         = $('body');

                if ($BODY.hasClass('nav-md')) {
                    $SIDEBAR_MENU.find('li.active ul').hide();
                } else {
                    $SIDEBAR_MENU.find('li.active-sm ul').show();
                }

                $BODY.toggleClass('nav-md nav-sm');

                setContentHeight();
            });

            element.on('$destroy', function () {
                element.off();
            });
        }
    };
}]);