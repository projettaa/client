'use strict';

app.directive('ngReplace', [function () {
    return {
        replace: true,
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.replaceWith(element.children());
        }
    }
}]);