'use strict';

app.constant('FlashMessages', {
    TEACHER   : {
        STORE_SUCCESS : "L'enseignement '{{lastName}}' a bien été créé.",
        STORE_FAILED  : "L'enseignement '{{lastName}}' n'a pas pu être créé. Veuillez rééssayer.",
        UPDATE_SUCCESS: "L'enseignement '{{lastName}}' a bien été modifié.",
        UPDATE_FAILED : "L'enseignement '{{lastName}}' n'a pas pu être modifié. Veuillez rééssayer.",
        DELETE_SUCCESS: "L'enseignement '{{lastName}}' a bien été supprimé.",
        DELETE_FAILED : "L'enseignement '{{lastName}}' n'a pas pu être supprimé. Veuillez rééssayer.",
        CONFIRM_DELETE: "Êtes-vous sûr de vouloir supprimer l'enseignement '{{lastName}}' ?.",

    },
    COMPANY   : {
        STORE_SUCCESS : "L'enseignement '{{entreprise}}' a bien été créé.",
        STORE_FAILED  : "L'enseignement '{{entreprise}}' n'a pas pu être créé. Veuillez rééssayer.",
        UPDATE_SUCCESS: "L'entreprise '{{lastName}}' a bien été modifié.",
        UPDATE_FAILED : "L'entreprise '{{entreprise}}' n'a pas pu être modifié. Veuillez rééssayer.",
        DELETE_SUCCESS: "L'entreprise '{{lastName}}' a bien été supprimé.",
        DELETE_FAILED : "L'entreprise '{{lastName}}' n'a pas pu être supprimé. Veuillez rééssayer.",
        CONFIRM_DELETE: "Êtes-vous sûr de vouloir supprimer l'entreprise '{{lastName}}' ?.",

    },
    INTERNSHIP: {
        STORE_SUCCESS : "Le stage a bien été créé.",
        STORE_FAILED  : "Le stage n'a pas pu être créé. Veuillez rééssayer.",
        UPDATE_SUCCESS: "Le stage a bien été modifié.",
        UPDATE_FAILED : "Le stage n'a pas pu être modifié. Veuillez rééssayer.",
        DELETE_SUCCESS: "Le stage a bien été supprimé.",
        DELETE_FAILED : "Le stage n'a pas pu être supprimé. Veuillez rééssayer.",
        CONFIRM_DELETE: "Êtes-vous sûr de vouloir supprimer ce stage ?.",
        CHOOSE_TEACHER: "Vous devez choisir un encadrant à affecter."
    },
    USER      : {
        STORE_SUCCESS : "L'utilisateur '{{username}}' a bien été créé.",
        STORE_FAILED  : "L'utilisateur '{{username}}' n'a pas pu être créé. Veuillez rééssayer.",
        UPDATE_SUCCESS: "L'utilisateur '{{username}}' a bien été modifié.",
        UPDATE_FAILED : "L'utilisateur '{{username}}' n'a pas pu être modifié. Veuillez rééssayer.",
        DELETE_SUCCESS: "L'utilisateur '{{username}}' a bien été supprimé.",
        DELETE_FAILED : "L'utilisateur '{{username}}' n'a pas pu être supprimé. Veuillez rééssayer.",
        CONFIRM_DELETE: "Êtes-vous sûr de vouloir supprimer l'utilisateur '{{username}}' ?."
    }
});