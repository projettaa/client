'use strict';

app.factory('UserService', ['$resource', 'Configuration', function ($resource, Configuration) {
    return $resource(Configuration.server + '/users/:id/:action', {id: '@id', action: '@action'}, {
        update: {method: 'PUT'}
    });
}]);