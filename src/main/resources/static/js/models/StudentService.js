'use strict';

app.factory('StudentService', ['$resource', 'Configuration', function ($resource, Configuration) {
    return $resource(Configuration.server + '/students/:id', {id: '@id'}, {
        update: {method: 'PUT'}
    });
}]);