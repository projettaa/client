'use strict';

app.factory('TeacherService', ['$resource', 'Configuration', function ($resource, Configuration) {
    return $resource(Configuration.server + '/teachers/:id', {id: '@id', action: '@action'}, {
        update: {method: 'PUT'}
    });
}]);

