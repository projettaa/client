'use strict';

app.factory('RoleService', ['$resource', 'Configuration', function ($resource, Configuration) {
    return $resource(Configuration.server + '/roles/:id', {id: '@id'}, {
        update: {method: 'PUT'}
    });
}]);