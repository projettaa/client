'use strict';

app.factory('InternshipService', ['$resource', 'Configuration', function ($resource, Configuration) {
    return $resource(Configuration.server + '/internships/:id/:action', {id: '@id', action: '@action'}, {
        update: {method: 'PUT'},
        findOne  : {method: 'GET'}
    });
}]);