'use strict';

app.factory('CompanyService', ['$resource', 'Configuration', function ($resource, Configuration) {
    return $resource(Configuration.server + '/companies/:id', {id: '@id', action: '@action'}, {
        update: {method: 'PUT'}
    });
}]);

