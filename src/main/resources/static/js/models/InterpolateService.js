'use strict';

app.factory('InterpolateService', ['$interpolate', 'Flash', function ($interpolate) {
    return function interpolate(message, object) {
        return $interpolate(message)(object);
    };
}]);