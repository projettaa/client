'use strict';

app.factory('FlashService', ['InterpolateService', 'Flash', function (InterpolateService, Flash) {
    function FlashService() {
    }

    FlashService.prototype.successAlert = function (message, object) {
        return this.alert('success', message, object);
    };

    FlashService.prototype.errorAlert = function (message, object) {
        return this.alert('danger', message, object);
    };

    FlashService.prototype.alert = function (type, message, object) {
        return Flash.create(type, InterpolateService(message, object));
    };

    return new FlashService();
}]);