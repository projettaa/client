'use strict';

app.factory('TimeUtilsService', [function () {
    function TimeUtilsService() {
    }

    TimeUtilsService.prototype.now = function (minutes) {
        var now = Date.now();
        minutes = parseInt(minutes);
        if (isNaN(minutes)) {
            return now;
        }
        var coeff = 1000 * 60 * minutes;
        return new Date(Math.round(now / coeff) * coeff).getTime();
    };

    return new TimeUtilsService();
}]);