'use strict';

app.factory('AuthService', ['$http', function ($http) {
    var Auth = function Auth() {
        this.authenticated = false;
        this.user          = null;
    };

    Auth.prototype.userDetails = function (end) {
        var self = this;
        return $http.get('/user')
                    .then(function (response) {
                        console.log(response);
                        self.authenticated = true;
                        self.user          = self.authenticated ? response.data : null;
                    }, function () {
                        self.authenticated = false;
                        self.user          = null;
                    })
                    .finally(end);
    };

    Auth.prototype.isAuthenticated = function (cb) {
        var self = this;
        return this.userDetails(function () {
            typeof cb === 'function' && cb(self.authenticated);
        });
    };

    Auth.prototype.logout = function (end) {
        var self = this;
        $http.post('logout').finally(function () {
            self.authenticated = false;
            typeof end === 'function' && end();
        });
    };

    return new Auth();
}]);