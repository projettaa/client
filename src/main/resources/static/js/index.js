'use strict';

var app = angular.module('library', ['ngRoute', 'ngResource', 'xeditable', 'ngFlash', 'ngAnimate', 'ui.mask', 'ui.bootstrap']);

app.config(['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider
        .when('/', {
            title: 'Stages',
            templateUrl: 'views/index.html',
            controller: 'HomeController'
        })
        .when('/internships', {
            title: 'Liste des stages | INT Management',
            templateUrl: 'views/internships/index.html',
            controller: 'InternshipController'
        })
        .when('/internships/create', {
            title: 'Ajouter un stage | INT Management',
            templateUrl: 'views/internships/create/index.html',
            controller: 'InternshipController'
        })
        .when('/internships/:id', {
            title: 'Afficher un stage | INT Management',
            templateUrl: 'views/internships/show/index.html',
            controller: 'InternshipController'
        })
        .when('/internships/:id/edit', {
            title: 'Modifier un stage | INT Management',
            templateUrl: 'views/internships/edit/index.html',
            controller: 'InternshipController'
        })
        .when('/profile', {
            title: 'Profile',
            templateUrl: 'views/profile/index.html',
            controller: 'ProfileController'
        })
        .when('/admin/users', {
            title: 'Utilisateurs | Administration',
            templateUrl: 'views/administration/users/index.html',
            controller: 'UserController'
        })
        .when('/admin/internships', {
            title      : 'Administration des stages | INT Management',
            templateUrl: 'views/administration/internships/index.html',
            controller : 'AdminInternshipController'
        })
        .when('/admin/internships/:id', {
            title      : 'Administrer un stage | INT Management',
            templateUrl: 'views/administration/internships/show/index.html',
            controller : 'AdminInternshipController'
        })
        .when('/admin/teachers', {
            title: 'Professeurs | Administration',
            templateUrl: 'views/administration/teachers/teachers.html',
            controller: 'TeacherController'
        })
        .when('/admin/companies', {
            title: 'Entreprises | Administration',
            templateUrl: 'views/administration/companies/companies.html',
            controller: 'CompanyController'
        })
        .otherwise({
            redirect: '/'
        });
    $locationProvider.html5Mode(true);

    $httpProvider.defaults.transformResponse.splice(0, 0, function (data, headersGetter) {
        if (data && headersGetter()['content-type'] && headersGetter()['content-type'].startsWith('application/json')) {
            return JSOG.parse(data);
        }

        return data;
    });

    $httpProvider.defaults.transformRequest.splice(0, 0, function (data, headersGetter) {
        if (data && headersGetter()['accept'] && headersGetter()['accept'].startsWith('application/json')) {
            data = angular.extend({}, data);
            return JSOG.stringify(data);
        }

        return data;
    });

    $httpProvider.interceptors.push('BearerAuthInterceptor');
}]);

app.run(['$rootScope', 'editableOptions', '$window', 'AuthService', function ($rootScope, editableOptions, $window, AuthService) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        if (current.hasOwnProperty('$$route')) {
            $rootScope.title = current.$$route.title;
        }
    });
    editableOptions.theme = 'bs3';
    // TODO : token has to be set when the user connects
    $window.localStorage.setItem('token', '65a11ceea12f97e8937a7ddf32f13ebc');

    $rootScope.userDetails = function () {
        AuthService.userDetails(function end() {
            $rootScope.user = AuthService.user;
            $rootScope.authenticated = AuthService.authenticated;
            console.log(AuthService);
            $rootScope.$broadcast('userDetails');
        });
    };

    $rootScope.userDetails();

    $rootScope.logout = function () {
        AuthService.logout(function () {
            $rootScope.authenticated = AuthService.authenticated;
            $location.path("/");
        });
    };

    $rootScope.hasRoles = function () {
        if ($rootScope.authenticated) {
            var roles = Array.prototype.slice.call(arguments);
            var userRole = $rootScope.user.role.name;
            return roles.some(function (role) {
                role = "ROLE_" + role;
                return userRole === role;
            });
        }
        return false;
    }
}]);