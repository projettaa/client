'use strict';

app.controller('StudentController', ['$scope', '$controller', 'StudentService', 'RoleService', 'TimeUtilsService', function ($scope, $controller, StudentService, RoleService, TimeUtilsService) {
    $scope.FLASH_KEY = "USER";
    $scope.roles     = [];

    angular.extend(this, $controller('BaseController', {
        $scope : $scope,
        Service: StudentService
    }));

    $scope.FLASH_INTERPOLATION = "username";

    $scope.refreshRoles = function (user) {
        user.idRoles = user.roles.map(function (role) {
            return role.id;
        });
    };

    RoleService.query().$promise.then(function (roles) {
        $scope.roles = roles;
    });

    $scope.showRoles = function (user) {
        return user.roles.length > 0 ? user.roles.map(function (role) {
            return role.slug;
        }).join(', ') : 'Aucun rôle';
    };

    $scope.newItem = function (id) {
        return {
            id       : id,
            username : '',
            password : '',
            firstName: '',
            lastName : '',
            roles    : []
        };
    };

    $scope.beforeStore = function (user) {
        user.roles = user.idRoles.map(function (idRole) {
            return {
                startTime: TimeUtilsService.now(5), // TODO: let the admin choose start and end time
                endTime  : null,
                role     : {
                    id: idRole
                }
            };
        });
        return user;
    };

    $scope.fixReferenceError = function (user) {
        // FIXME: Object id reference error because for example id [33] does not exist
        user.roles = user.idRoles && user.idRoles.map(function (idRole) {
                return {
                    startTime: TimeUtilsService.now(5), // TODO: let the admin choose start and end time
                    endTime  : null,
                    role     : {
                        id: idRole
                    }
                };
            }) || [];
        user.events.forEach(function (event) {
            event.eventManager = {
                id: event.eventManager.id
            };
            if (event.room) {
                event.room = {
                    id: event.room.id
                };
            }
            event.users = event.users.map(function (user) {
                return {
                    id: user.id
                };
            })
        });
        return user;
    };

    $scope.update = function (user) {
        $scope.updateAlias(user, function (user) {
            $scope.refreshRoles(user);
        });
    };
}]);