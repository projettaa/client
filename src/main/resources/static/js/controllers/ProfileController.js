'use strict';

app.controller('ProfileController', ['$rootScope', '$scope', 'UserService', function ($rootScope, $scope, UserService) {
    $scope.me = $rootScope.user; // connected user

    $scope.$on('userDetails', function () {
        $scope.me = $rootScope.user;
    });

    $scope.saveUser = function () {
        var user = angular.extend({}, $scope.me);
        delete user.$promise;

        user = new UserService(user);
        user.$update();
    }
}]);