'use strict';

app.controller('CompanyController', ['$scope', '$controller', 'CompanyService', 'UserService', 'FlashMessages', 'FlashService', 'TimeUtilsService', function ($scope, $controller, CompanyService, UserService, FlashMessages, FlashService, TimeUtilsService) {
    $scope.FLASH_KEY = "COMPANY";
    $scope.FLASH_INTERPOLATION = "name";

    angular.extend(this, $controller('BaseController', {
        $scope: $scope,
        Service: CompanyService
    }));

    $scope.newItem = function (id) {
        return {
            ID: id,
            SIRET: '',
            name: '',
            adress: ''
        };
    };

    $scope.fixReferenceError = function (company) {

        return company;
    };

    $scope.open = function ($event, elementOpened, opened) {
        $event.preventDefault();
        $event.stopPropagation();

        opened[elementOpened] = !opened[elementOpened];
    };

    /**
     * @return {number}
     */
    $scope.Date = function () {
        return TimeUtilsService.now();
    };

}]);