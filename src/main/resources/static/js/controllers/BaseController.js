'use strict';

app.controller('BaseController', ['$rootScope', '$scope', 'Service', 'UserService', 'FlashMessages', 'FlashService', 'InterpolateService', 'queryItems', function ($rootScope, $scope, Service, UserService, FlashMessages, FlashService, InterpolateService, queryItems) {
    $scope.FLASH_INTERPOLATION = "title";
    $scope.items               = [];
    $scope.me                  = $rootScope.user; // connected user

    $scope.$on('userDetails', function () {
        $scope.me = $rootScope.user;
        $scope.queryItems();
        $scope.$broadcast('me');
    });

    $scope.queryItems = function () {
        if (!angular.isFunction(queryItems)) {
            Service.query().$promise.then(function (items) {
                $scope.items = items;
            });
        } else if ($scope.me) {
            queryItems($scope.me).$promise.then(function (items) {
                // FIXME: problem with internships that are retrieved with UserService
                $scope.items = items.map(function (item) {
                    return new Service(angular.copy(item));
                });
            })
        }
    };

    $scope.queryItems();

    $scope.create = function () {
        if (!$scope.inserted) {
            var id          = $scope.items.length > 0 ? $scope.items[$scope.items.length - 1].id + 1 : 1;
            $scope.inserted = $scope.newItem(id);
            $scope.items.push($scope.inserted);
        }
    };

    $scope.saveOrUpdate = function (item) {
        if ($scope.inserted) {
            $scope.store(item);
        } else {
            $scope.update(item);
        }
    };

    $scope.store = function (item) {
        delete item.id;
        item                             = $scope.beforeStore(item);
        item                             = new Service(item);
        var data                         = {};
        data[$scope.FLASH_INTERPOLATION] = item[$scope.FLASH_INTERPOLATION];
        item.$save().then(function (item) {
            $scope._store(item);
            var index = $scope.items.indexOf($scope.inserted);
            if (index !== -1) {
                $scope.items.splice(index, 1);
            }
            $scope.inserted = null;
            FlashService.successAlert(FlashMessages[$scope.FLASH_KEY].STORE_SUCCESS, data);
        }, function () {
            FlashService.errorAlert(FlashMessages[$scope.FLASH_KEY].STORE_FAILED, data);
        });
    };

    $scope.beforeStore = function (item) {
        return item;
    };

    $scope._store = function (item) {
        $scope.items.push(item);
    };

    $scope.delete = function (item, cb) {
        if (confirm(InterpolateService(FlashMessages[$scope.FLASH_KEY].CONFIRM_DELETE, {title: item.title}))) {
            var data                         = {};
            data[$scope.FLASH_INTERPOLATION] = item[$scope.FLASH_INTERPOLATION];
            item.$delete().then(function () {
                if (typeof cb === 'function') {
                    cb(item);
                } else {
                    $scope._delete(item);
                }
                FlashService.successAlert(FlashMessages[$scope.FLASH_KEY].DELETE_SUCCESS, data);
            }, function () {
                FlashService.errorAlert(FlashMessages[$scope.FLASH_KEY].DELETE_FAILED, data);
            });
        }
    };

    $scope._delete = function (item) {
        $scope.__delete($scope.items, $scope.cbId(item.id));
    };

    $scope.__delete = function (array, p) {
        $scope._remove($scope.findIndex(array, p), array);
    };

    $scope._remove = function (index, array) {
        if (index != -1) {
            array.splice(index, 1);
        }
    };

    $scope.findIndex = function (array, p) {
        return array.findIndex(p);
    };

    $scope.cbId = function (id) {
        return function (b) {
            return b.id === id;
        }
    };

    $scope.set = function (item) {
        var index = $scope.findIndex($scope.items, $scope.cbId(item.id));
        if (index != -1) {
            $scope.items[index] = item;
        }
    };

    $scope.update = $scope.updateAlias = function (item, success) {
        // FIXME: Object id reference error because for example id [33] does not exist
        item                             = $scope.fixReferenceError(item);
        var data                         = {};
        data[$scope.FLASH_INTERPOLATION] = item[$scope.FLASH_INTERPOLATION];
        item.$update().then(function () {
            if (typeof success === 'function') {
                success(item);
            }
            FlashService.successAlert(FlashMessages[$scope.FLASH_KEY].UPDATE_SUCCESS, data);
        }, function () {
            FlashService.errorAlert(FlashMessages[$scope.FLASH_KEY].UPDATE_FAILED, data);
        });
    };
}]);