'use strict';

app.controller('InternshipController', ['$scope', '$controller', 'InternshipService', 'UserService', 'FlashMessages', 'FlashService', '$location', '$routeParams', function ($scope, $controller, InternshipService, UserService, FlashMessages, FlashService, $location, $routeParams) {
    $scope.FLASH_KEY  = "INTERNSHIP";
    $scope.internship = {};

    angular.extend(this, $controller('BaseController', {
        $scope    : $scope,
        Service   : InternshipService,
        queryItems: function (me) {
            return UserService.query({id: me.id, action: 'internships'});
        }
    }));

    if ($routeParams.id) {
        InternshipService.findOne({id: $routeParams.id}).$promise.then(function (internship) {
            $scope.internship           = internship;
            $scope.internship.startDate = new Date($scope.internship.startDate);
            $scope.internship.endDate   = new Date($scope.internship.endDate);
        }, function () {

        });
    }

    $scope.newItem = function () {
        return {
            mission      : '',
            address      : '',
            gratification: 0,
            startDate    : new Date(),
            endDate      : new Date(),
            numberOfWeeks: 0,
            numberOfDays : 0,
            levelOfStudy : '',
            company      : {
                siret  : '',
                name   : '',
                address: ''
            },
            tutor        : {
                registrationNumber: '',
                lastName          : '',
                firstName         : '',
                mail              : '',
                phone             : '',
                'function'        : ''
            }
        };
    };

    $scope.inserted = $scope.newItem();

    $scope.create = function () {
        var internship = new InternshipService($scope.inserted);
        internship.$save().then(function () {
            $location.url('/internships');
        }, function () {
            FlashService.errorAlert(FlashMessages[$scope.FLASH_KEY].STORE_FAILED, {});
        });
    };

    $scope.update = function () {
        Object.keys($scope.internship).length > 0 && $scope.internship.$update().then(function () {
            FlashService.successAlert(FlashMessages[$scope.FLASH_KEY].UPDATE_SUCCESS, {});
        }, function () {
            FlashService.errorAlert(FlashMessages[$scope.FLASH_KEY].UPDATE_FAILED, {});
        });
    };

    $scope.pdfExport = function () {
        html2canvas($('#exportable').get(0), {
            onrendered: function (canvas) {
                var contentDataURL = canvas.toDataURL('image/jpeg');
                var pdf            = new jsPDF();
                pdf.addImage(contentDataURL, 'JPEG', 0, 0);
                pdf.save('Internship-' + $scope.internship.number)
            }
        });
    }
}]);