'use strict';

app.controller('TeacherController', ['$scope', '$controller', 'TeacherService', 'UserService', 'FlashMessages', 'FlashService', 'TimeUtilsService', function ($scope, $controller, TeacherService, UserService, FlashMessages, FlashService, TimeUtilsService) {
    $scope.FLASH_KEY = "TEACHER";
    $scope.FLASH_INTERPOLATION = "lastName";

    angular.extend(this, $controller('BaseController', {
        $scope: $scope,
        Service: TeacherService
    }));

    $scope.newItem = function (id) {
        return {
            ID: id,
            registrationNumber: '',
            lastName: '',
            firstName: '',
            mail: '',
            disciplined: ''
        };
    };

    $scope.fixReferenceError = function (teacher) {
        // In any case book can update teacher so it's not a problem
        // however, maybe there is a better way to do it
        // teacher = null;
        return teacher;
    };

    $scope.open = function ($event, elementOpened, opened) {
        $event.preventDefault();
        $event.stopPropagation();

        opened[elementOpened] = !opened[elementOpened];
    };

    /**
     * @return {number}
     */
    $scope.Date = function () {
        return TimeUtilsService.now();
    };

}]);