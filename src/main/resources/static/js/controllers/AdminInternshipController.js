'use strict';

app.controller('AdminInternshipController', ['$scope', '$controller', 'InternshipService', 'UserService', 'FlashMessages', 'FlashService', '$location', '$routeParams', 'TeacherService', function ($scope, $controller, InternshipService, UserService, FlashMessages, FlashService, $location, $routeParams, TeacherService) {
    $scope.FLASH_KEY  = "INTERNSHIP";
    $scope.internship = {};
    $scope.teachers   = [];

    angular.extend(this, $controller('BaseController', {
        $scope : $scope,
        Service: InternshipService
    }));

    TeacherService.query().$promise.then(function (items) {
        $scope.teachers = items;
    });

    if ($routeParams.id) {
        InternshipService.findOne({id: $routeParams.id}).$promise.then(function (internship) {
            $scope.internship           = internship;
            $scope.internship.startDate = new Date($scope.internship.startDate);
            $scope.internship.endDate   = new Date($scope.internship.endDate);
        }, function () {

        });
    }

    $scope.update = function () {
        Object.keys($scope.internship).length > 0 && $scope.internship.$update().then(function () {
            FlashService.successAlert(FlashMessages[$scope.FLASH_KEY].UPDATE_SUCCESS, {});
        }, function () {
            FlashService.errorAlert(FlashMessages[$scope.FLASH_KEY].UPDATE_FAILED, {});
        });
    };

    $scope.assign = function () {
        console.log($scope.internship);
        if ($scope.internship.teacher) {
            $scope.update();
        } else {
            FlashService.errorAlert(FlashMessages[$scope.FLASH_KEY].CHOOSE_TEACHER, {});
        }
    }

}]);