package tk.intmanagement.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.regex.Pattern;

@Component
@Order
public class RedirectFilter extends OncePerRequestFilter {
    @Autowired
    private ZuulProperties properties;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Map<String, ZuulProperties.ZuulRoute> routes = properties.getRoutes();
        final String requestURI = request.getRequestURI();
        final boolean[] match = {false};

        routes.forEach((s, zuulRoute) -> {
            Pattern pattern = Pattern.compile(zuulRoute.getPath().replaceAll("/\\*", "/?*").replaceAll("\\*", ".*"));
            match[0] = match[0] || pattern.matcher(requestURI).matches();
        });

        Pattern pattern = Pattern.compile("^.*?\\..*$");
        if (!(pattern.matcher(requestURI).matches() || match[0])) {
            RequestDispatcher rd = request.getRequestDispatcher("/");
            rd.forward(request, response);
        } else {
            filterChain.doFilter(request, response);
        }
    }
}
