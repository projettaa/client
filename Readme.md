## Description

- Client : application en angularjs



## Requirements

- Node  (http://nodejs.org/)

- Npm   (https://www.npmjs.com/)

- Bower (https://bower.io/)

- Gulp  (http://gulpjs.com/)




## Installation

Installation des dépendances par les commandes

```sh
$ npm install

$ bower install

$ gulp
```

## Usage

Pour lancer le projet, exécutez la commande suivante

```sh
$ mvn clean

$ mvn install
```

Puis lancer l'application client



## Déploiement

Pour déployer le projet, exécutez la commande

```sh
$ mvn package
```

Ensuite, déplacez le paquet war vers le répertoire webapps Tomcat



## License
Copyright (c) 2016 Projet TAA, ISTIC, Univ-Rennes 1